import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { fetchInfo } from '../actions/postAction';
import { fetchTable } from '../actions/fetchTableAction';
import ReactTable from 'react-table';

import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { Card, CardBody, Col, Button, Container, Table } from 'reactstrap';
import { Row } from 'react-grid-system';
import 'bootstrap/dist/css/bootstrap.css';
import "react-table/react-table.css";

import  toysrus_logo  from '../../src/toysrus_logo.gif'

import { alertText, renderBarCode } from './Constants';
const NullComponent = () => null;
class Postform extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 'RegistryNum': '', 'AlreadyPurchased': 0 };
  }
  onSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    this.setState({ RegistryNum: data.get('registry_num') });
    this.props.fetchInfo(data.get('registry_num'));
    this.props.fetchTable(data.get('registry_num'));
  }


  renderCustomerInfo = () => (
    <div>
      <Row>
        <Col ><b >Registry #:</b> {this.state.RegistryNum}</Col>
      </Row>
      <Row>
        <Col><b >Maiden/Alternate Name:</b> {this.props.maidenName}</Col>
      </Row>
      <Row>
        <Col><b >{this.props.firstName} {this.props.lastName}'s Message: </b>{this.props.comments}</Col>
      </Row>
      <Row>
        <Col><b >Location:</b> {this.props.address} , {this.props.zip}, {this.props.city}, {this.props.state}</Col>
      </Row>
      <Row>
        <Col><b > Expected Arrival Date:</b>{this.props.eventDate}</Col>
      </Row>
      <br></br>
    </div>
  );

  TableItemsAvaOnlineInstore = (secondItem, index) => (

    <Container key={index}>

      <Card key={index}>

        <div className='md-12' key={index}>

          <ReactTable key={index}

            columns={

              [
                {
                  Header: () => (
                    <span>
                      <b> {secondItem[0].itemCategoryDescription}:({secondItem.length})</b>
                    </span>
                  ),
                  id: 'CatIteam',
                  className: "leftify",
                  columns: [
                    {
                      Header: '',
                      id: "Image",
                      width: 300,
                      accessor: 'imageUrl',
                      Cell: (row) =>
                        <div><img className="fixed_img" src={row}/></div>
                    }, {
                      Header: 'Description',
                      id: 'Description',
                      Cell: row =>
                        <span >
                          <div>
                            {row.original.productDescription}
                          </div>
                          <div>By: {row.original.manufacturerDescription}</div>
                          <div>
                            Item #: {row.original.skn}
                          </div>
                        </span> // Custom cell components!
                    }, {
                      Header: 'Price',
                      accessor: 'retail',
                      width: 100,
                      className: "center"
                    }, {
                      accessor: 'itemRequested', // Required because our accessor is not a string
                      Header: 'Desired',
                      width: 100,
                      className: "center"
                    }, {
                      Header: 'Purchased', // Custom header components!
                      accessor: 'itemPurchased',
                      width: 100,
                      className: "center"
                    }]
                }]}
            data={secondItem}
            className="-striped -highlight"
            showPagination={false}
            defaultPageSize={this.props.itemsAvaOnlineInstoreSize}
   

          />

        </div>
      </Card>
    </Container>
  );

  TableOnlineOnly = (secondItem, index) => (
  
    <Container key={index}>

      <Card key={index}>

        <div className='md-12' key={index}>
          <ReactTable key={index}
            columns={

              [{
                Header: () => (
                  <span>

                    <b>{secondItem[0].itemCategoryDescription}:({secondItem.length})</b>
                  </span>
                ),
                id: 'CatIteam',
                className: "leftify",
                columns: [
                  {
                    Header: '',
                    id: "Image",
                    width: 300,
                    accessor: 'imageUrl',
                    Cell: (row) =>
                      <div><img className="fixed_img"  src={row} /></div>
                  }, {
                    Header: 'Description',
                    id: 'Description',
                    Cell: row =>
                      <span >
                        <div>
                          {row.original.productDescription}
                        </div>
                        <div>By: {row.original.manufacturerDescription}</div>
                        <div>
                          Item #: {row.original.skn}
                        </div>
                      </span> // Custom cell components!
                  }, {
                    Header: 'Price',
                    accessor: 'retail',
                    width: 100,
                    className: "center"
                  }, {
                    accessor: 'itemRequested', // Required because our accessor is not a string
                    Header: 'Desired',
                    width: 100,
                    className: "center"
                  }, {
                    Header: 'Purchased', // Custom header components!
                    accessor: 'itemPurchased',
                    width: 100,
                    className: "center"
                  }]
              }]}
            data={secondItem}
            className="-striped -highlight"
            showPagination={false}
            defaultPageSize={this.props.itemsOnlineOnlySize}
          />

        </div>
      </Card>
    </Container>
  );

  renderTableAlreadyPurchased = (alreayPurch,index) => (
    <Container key={index}>

    <Card key={index}>

      <div className='md-12' key={index}>
        <ReactTable key={index}
            columns={
              [{

                Header: () => (
                  <span>
                    <b> <i className="fa-tasks" /> Items Already Purchased:({this.props.itemsAlreadyPurchased})</b>
                  </span>
                ),
                headerClassName: 'my-favorites-column-header-group',
                columns: [
                  {
                    Header: 'Description',
                    id: 'Description',

                    Cell: row =>
                      <span >
                        <div>
                          {row.original.productDescription}
                        </div>
                        <div>By: {row.original.manufacturerDescription}</div>
                        <div>
                          Item #: {row.original.skn}
                        </div>
                      </span> // Custom cell components!
                  },
                   {
                    accessor: 'itemRequested', // Required because our accessor is not a string
                    Header: 'Desired',
                    className: "center"
                  },
                   {
                    Header: 'Purchased', // Custom header components!
                    accessor: 'itemPurchased',
                    className: "center"
                  }]

              }]}
            data={alreayPurch}
            className="-striped -highlight"
            showPagination={false}
            NoDataComponent={NullComponent}
            defaultPageSize={this.props.itemsAlreadyPurchased}
          />

        </div>
      </Card>
    </Container>
  );



  sortedTableItemsAvaOnlineInstore = () => {
    return (
      <div>
        <h6>Items In Store Only:  {this.props.itemsAvaOnlineInstoreSize}</h6>
        {this.props.itemsAvaOnlineInstore.map((item, index) => (
          this.TableItemsAvaOnlineInstore(item, index)
        ))}</div>
    )
  };

  sortedTableOnlineOnly = () => {
    return (
      <div>
        <h6>Items available Online: {this.props.itemsOnlineOnlySize} </h6>
        {this.props.itemsOnlineOnly.map((item, index) => (
          this.TableOnlineOnly(item, index)
        ))}</div>)
  };

  sortedAlreadyPurchased = () =>{
    console.log(this.props.itemsAlreadyPurchased)
    return (
      <div>
      {
    this.props.alreadyPurchasedItem.map((item, index) => (
      this.renderTableAlreadyPurchased(item, index)
      ))}</div>)
  };

     
     

  render() {

    return (

      <Col md={12}>
        <Card>
          <CardBody>
            <Container>
            <img src={toysrus_logo}></img>
              <form onSubmit={this.onSubmit}>
                <div >

                  <FormGroup controlId="formValidationSuccess1" validationState="success">
                    <ControlLabel>Registry Number</ControlLabel>
                    <FormControl type="text" name="registry_num" />
                  </FormGroup>
                  <Button color="primary" type="submit">
                    Search
                  </Button>
                  <Row>
                    <Col>{this.props.loading && this.renderCustomerInfo()}</Col>
                    <Col md="auto"></Col>
                    <Col xs lg="2">
                      {this.props.loadingtable && renderBarCode()}

                    </Col>
                  </Row>
                </div>
              </form>
              {this.props.displayNotFound && (alertText(this.props.posterror.message))}
              {this.props.regNotFound && (alertText(this.props.posterror))}

              {this.props.loadingtable && this.sortedTableItemsAvaOnlineInstore()}
              {this.props.loadingtable && this.sortedTableOnlineOnly()}
              {this.props.loadingtable && this.sortedAlreadyPurchased()}

            </Container>
          </CardBody>
        </Card>
      </Col>
    );
  }
}



const mapDispatchToProps = (dispatch) => {
  console.log('mapDispatchToProps dispatch', dispatch);
  return bindActionCreators({ fetchInfo, fetchTable }, dispatch);
};


const mapStateToProps = (state) => {

  return {
    comments: state.posts.comments,
    maidenName: state.posts.maidenName,
    firstName: state.posts.firstName,
    lastName: state.posts.lastName,
    city: state.posts.city,
    state: state.posts.state,
    address: state.posts.address,
    zip: state.posts.zip,
    posterror: state.table.errortable,
    loadingtable: state.table.loadingtable,
    displayNotFound: state.table.displayNotFound,
    loading: state.posts.loading,
    itemsAvaOnlineInstore: state.table.itemsAvaOnlineInstore,
    itemsOnlineOnly: state.table.itemsOnlineOnly,
    alreadyPurchasedItem: state.table.alreadyPurchased,
    alreadyPurchasedCounter: state.table.alreadyPurchasedCounter,
    regNotFound: state.table.regNotFound,
    itemsAvaOnlineInstoreSize: state.table.itemsAvaOnlineInstoreSize,
    itemsOnlineOnlySize: state.table.itemsOnlineOnlySize,
    eventDate: state.posts.eventDate,
    itemsAlreadyPurchased: state.table.itemsAlreadyPurchased
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Postform);
