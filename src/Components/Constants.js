import React, { Component}  from 'react';
import {  Alert } from 'reactstrap';
import bwipjs from 'bwip-js';
 

export const alertText = (value) => (
    <div>
    <Alert color="danger">
    {value}
  </Alert>
</div>
  );


 export const renderBarCode = () => (
    <canvas id="mycanvas"></canvas>

);
export const createBarCode = (value) => (
        bwipjs('mycanvas', {
          bcid: 'code128',
          text: value,
          scale: 3,
          height: 8,
          width: 20,
           includetext: true,    
          textsize: 13,
          margin: 20,
          marginLeft: 20,
          textfont:    'Inconsolata', 
           
        }, (err) => {
          if (err) {
            console.log('err'+err);
          }  
        })
      );
