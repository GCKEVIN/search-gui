import { createStore, applyMiddleware,combineReducers ,compose} from 'redux';
import thunk from 'redux-thunk';
import promise from "redux-promise-middleware"
import {fetchTableReducer,postReducer} from './reducers/index'
 

const reducer = combineReducers({
    posts: postReducer,
    table: fetchTableReducer
});
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancer(applyMiddleware(promise(), thunk)));
export default store;