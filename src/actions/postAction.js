import { FETCH_INFO_SUCCESS, FETCH_INFO_FAILURE,PROPERTIES } from './type';
 import axios from 'axios';

 


export const fetchRegisterInfoSuccess = (listInfo) => ({
    type: FETCH_INFO_SUCCESS,
    payload: {
        listInfo
    }
});
export const fetchRegisterInfoFailure = error => ({
    type: FETCH_INFO_FAILURE,
    payload: { error }
});


export function fetchInfo(value) {

    return (dispatch) => {
        axios({
            method: 'post',
            url: PROPERTIES.clientInfoUrl,
            timeout: 1000 * 20, // Wait for 5 seconds
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
            },
            data: {
                locale: {
                    country: 'CA',
                    language: 'en',
                },
                messageId: '11111',
                registryNumber: value,
            }
        })
            .then((res) => {
                if (res.data.responseStatus.indicator === 'SUCCESS') {
                    dispatch(fetchRegisterInfoSuccess(res.data))
                   
                }
                else {
                    dispatch(fetchRegisterInfoFailure(res.data))
                }
            })
            .catch(error => {
                console.log('Error' + error);

                dispatch(fetchRegisterInfoFailure(error))
            });
    }
};

