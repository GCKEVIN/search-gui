import { FETCH_TABLE_FAILURE, FETCH_TABLE_SUCCESS, REGISTRY_NUM_NOT_FOUND, PROPERTIES } from './type';

import axios from 'axios';
import { createBarCode } from '../Components/Constants';

export const fetchTableInfoSuccess = (alreadyPurchased, itemsAvaOnlineInstore, itemsOnlineOnly, itemsAvaOnlineInstoreSize, itemsOnlineOnlySize, itemsAlreadyPurchased) => ({
    type: FETCH_TABLE_SUCCESS,
    payload: {
        alreadyPurchased,
        itemsAvaOnlineInstore,
        itemsOnlineOnly,
        itemsAvaOnlineInstoreSize,
        itemsOnlineOnlySize,
        itemsAlreadyPurchased
    }
});
export const fetchTableInfoFailure = error => ({
    type: FETCH_TABLE_FAILURE,
    payload: { error }
});
export const fetchTableRegNotFound = error => ({
    type: REGISTRY_NUM_NOT_FOUND,
    payload: { error }
});
function groupBy(array, f) {
    var groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}
export function fetchTable(value) {

    return (dispatch) => {
        axios({
            method: 'post',
            url: PROPERTIES.itemInfoUrl,
            timeout: 1000 * 20, // Wait for 5 seconds

            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
            },
            data: {

                storeNumber: '3502',
                registryNumber: value,
                registryType: 'B',
                locale: {
                    country: 'CA',
                    language: 'EN'
                }
            }
        }).then((res) => {

            let notPurchased = [];
            let alreadyPurchased = [];
            let itemsAvaOnlineInstore = [];
            let itemsOnlineOnly = [];
            let itemsAvaOnlineInstoreSize = 0;
            let itemsOnlineOnlySize = 0;
            let itemsAlreadyPurchased = 0;
            //check if registry num contains items
            if (res.data.anyType.length > 0) {

                //seperate items into already purchased and not purchased yet.
                res.data.anyType.map((item, i) => {
                    if (item.itemPurchased === item.itemRequested) {
                        alreadyPurchased.push(item);
                    }
                    else {
                        notPurchased.push(item);
                    }
                }
                );


                //seperate items into online only and available instore and online
                notPurchased.map((item, i) => {
                    if (item.availableInStore === true) {
                        itemsAvaOnlineInstore.push(item);
                    }
                    else {
                        itemsOnlineOnly.push(item);
                    }
                });
                itemsAvaOnlineInstoreSize = itemsAvaOnlineInstore.length;
                itemsOnlineOnlySize = itemsOnlineOnly.length;
                itemsAlreadyPurchased = alreadyPurchased.length;
                //order items 
                itemsAvaOnlineInstore = groupBy(itemsAvaOnlineInstore, function (item) {
                    return [item.itemCategoryCode];
                });
                itemsOnlineOnly = groupBy(itemsOnlineOnly, function (item) {
                    return [item.itemCategoryCode];
                });


                alreadyPurchased = groupBy(alreadyPurchased, function (item) {
                    return [item.color];
                });

                dispatch(fetchTableInfoSuccess(alreadyPurchased, itemsAvaOnlineInstore, itemsOnlineOnly, itemsAvaOnlineInstoreSize, itemsOnlineOnlySize, itemsAlreadyPurchased))
                createBarCode(value);
            }
            else {
                dispatch(fetchTableRegNotFound(REGISTRY_NUM_NOT_FOUND))
            }
        })
            .catch(error => {
                console.log('Error' + error);
                dispatch(fetchTableInfoFailure(error))
            });
    }
};
