
export const FETCH_INFO_BEGIN = 'FETCH_INFO_BEGIN';
export const FETCH_INFO_SUCCESS = 'FETCH_INFO_SUCCESS';
export const FETCH_INFO_FAILURE = 'FETCH_INFO_FAILURE';

export const FETCH_TABLE_SUCCESS = 'FETCH_TABLE_SUCCESS';
export const FETCH_TABLE_FAILURE = 'FETCH_TABLE_FAILURE';
export const REGISTRY_NUM_NOT_FOUND ='REGISTRY_NUM_NOT_FOUND';
export const PROPERTIES = {
     itemInfoUrl : 'http://192.168.2.14:9095/api/registry/reg-service/get-product-listing',
     clientInfoUrl : 'http://192.168.2.14:9095/api/registry/reg-bus/get-registry-details',
     imageHost: '',
     imagePort: ''
};