import {  FETCH_INFO_SUCCESS ,FETCH_INFO_FAILURE} from '../actions/type';
 
 
export default function (state={} , action) {
 
    switch (action.type) {
   
        case FETCH_INFO_SUCCESS:
            return { ...state,
                loading: true, 
                maidenName: action.payload.listInfo.registryDetail.people[0].publicDetails.maidenName,
                firstName: action.payload.listInfo.registryDetail.people[0].publicDetails.firstName,
                lastName: action.payload.listInfo.registryDetail.people[0].publicDetails.lastName,
                city: action.payload.listInfo.registryDetail.people[0].publicDetails.city,
                state: action.payload.listInfo.registryDetail.people[0].publicDetails.state,
                address: action.payload.listInfo.registryDetail.people[0].privateDetails.addressOne,
                comments:action.payload.listInfo.registryDetail.comments,
                zip:action.payload.listInfo.registryDetail.people[0].privateDetails.zip,
                eventDate:action.payload.listInfo.registryDetail.eventDate
            };
        case FETCH_INFO_FAILURE:
            return { ...state,loading: false, error: action.payload.error };
        default:
            return state;
    }
}

 
