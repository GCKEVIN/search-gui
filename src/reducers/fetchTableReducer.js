import { FETCH_TABLE_FAILURE, FETCH_TABLE_SUCCESS, REGISTRY_NUM_NOT_FOUND } from '../actions/type';
const initalTable = {
    alreadyPurchased: [],
    loadingtable: false,
    errortable: null,
    displayNotFound: false,
    itemsAvaOnlineInstore: [],
    itemsOnlineOnly: [],
    alreadyPurchasedCounter: 0,
    regNotFound: false,
    itemsOnlineOnlySize: 0,
    itemsAvaOnlineInstoreSize: 0,
    itemsAlreadyPurchased: 0

}

export default function (state = initalTable, action) {
 
    switch (action.type) {

        case FETCH_TABLE_SUCCESS:
            return {
                ...state, loadingtable: true,
                alreadyPurchased: action.payload.alreadyPurchased, itemsAvaOnlineInstore: action.payload.itemsAvaOnlineInstore,
                itemsOnlineOnly: action.payload.itemsOnlineOnly, itemsAvaOnlineInstoreSize: action.payload.itemsAvaOnlineInstoreSize,
                itemsOnlineOnlySize: action.payload.itemsOnlineOnlySize,itemsAlreadyPurchased:action.payload.itemsAlreadyPurchased,
                 regNotFound: false,displayNotFound:false
            };
        case FETCH_TABLE_FAILURE:
            return { ...state, loadingtable: false, errortable: action.payload.error, displayNotFound: true, regNotFound: false };
        case REGISTRY_NUM_NOT_FOUND:
            return { ...state, loadingtable: false, errortable: action.payload.error, regNotFound: true };
        default:
            return state;
    }
}

