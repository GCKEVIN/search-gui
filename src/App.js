import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import Postform from './Components/Postform';
import store from './store';


  
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
    
        <div>
        <Postform></Postform>

        </div>
      </Provider>
    );
  }
}

export default App;
